package fr.epsi.b3.dal;

import fr.epsi.b3.domain.Person;

import java.sql.*;
import java.util.List;
import java.util.ResourceBundle;

public class PersonDAO implements IDAO<Person> {
	
	private static final String INSERT_QUERY = "INSERT INTO person (email, name) VALUES (?, ?)";
	private static final String FIND_BY_ID_QUERY = "SELECT * from person WHERE id = ?";
	
	private static String url;
	private static String login;
	private static String password;
	
	static {
		ResourceBundle bundle = ResourceBundle.getBundle( "db" );
		url = bundle.getString( "db.url" );
		login = bundle.getString( "db.login" );
		password = bundle.getString( "db.password" );
	}
	
	@Override
	public Person findById( int id ) throws SQLException {
		
		Person person = null;
		try ( Connection connection = DriverManager.getConnection( url, login, password );
			  PreparedStatement pst = connection.prepareStatement( FIND_BY_ID_QUERY ) ) {
			
			pst.setInt( 1, id );
			
			try ( ResultSet rs = pst.executeQuery() ) {
				if ( rs.next() ) {
					person = new Person( rs.getInt( "id" ), rs.getString( "name" ), rs.getString( "email" ) );
				}
			}
		}
		return person;
	}
	
	@Override
	public List<Person> findAll() {
		return null;
	}
	
	@Override
	public void create( Person person ) throws SQLException {
		
		Connection connection = DriverManager.getConnection( url, login, password );
		PreparedStatement pst = connection.prepareStatement( INSERT_QUERY, Statement.RETURN_GENERATED_KEYS );
		pst.setString( 1, person.getEmail() );
		pst.setString( 2, person.getName() );
		pst.executeUpdate();
		
		ResultSet rs = pst.getGeneratedKeys();
		if ( rs.next() ) {
			person.setId( rs.getInt( "id" ) );
		}
		rs.close();
		pst.close();
		connection.close();
	}
	
	@Override
	public void update( Person object ) {
	
	}
	
	@Override
	public void delete( Person object ) {
	
	}
}
