package fr.epsi.b3.dal;

import java.sql.SQLException;
import java.util.List;

public interface IDAO<T> {
	
	T findById( int id ) throws SQLException;
	
	List<T> findAll();
	
	void create( T object ) throws SQLException;
	
	void update( T object );
	
	void delete( T object );
}
