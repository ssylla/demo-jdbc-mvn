package fr.epsi.b3;

import fr.epsi.b3.dal.PersonDAO;
import fr.epsi.b3.domain.Person;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;

public class App {
	
	public static void main( String[] args ) {
		
		Person person = new Person( "Hugo 2 T.", "hto@ssy.org" );
		PersonDAO dao = new PersonDAO();
		try {
			dao.create( person );
			System.out.println(person);
			
			Person person1 = dao.findById( 4 );
			System.out.println(person1);
		} catch ( SQLException e ) {
			System.out.println(e.getMessage());
		}
	}
}
