package fr.epsi.b3.domain;

public class Person {
	
	private int id;
	private String name;
	private String email;
	
	public Person() {}
	
	public Person( String name, String email ) {
		this.name = name;
		this.email = email;
	}
	
	public Person( int id, String name, String email ) {
		this.id = id;
		this.name = name;
		this.email = email;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId( int id ) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName( String name ) {
		this.name = name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail( String email ) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Person{" );
		sb.append( "id=" ).append( id );
		sb.append( ", name='" ).append( name ).append( '\'' );
		sb.append( ", email='" ).append( email ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
